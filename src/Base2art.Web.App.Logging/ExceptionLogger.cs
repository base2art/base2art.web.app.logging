namespace Base2art.Web.App.Logging
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using ExceptionHandling;
    using Web.App.Logging;

    public class ExceptionLogger : IExceptionLogger
    {
        private readonly string basePath;
        private readonly string siteName;

        public ExceptionLogger(string siteName, string basePath)
        {
            this.siteName = siteName;
            this.basePath = basePath;
        }

        public Task HandleException(Exception e)
        {
            if (!e.ShouldLog())
            {
                return Task.CompletedTask;
            }

            var sitePath = LoggingPaths.LogDir("Errors", this.siteName, this.basePath);
            var path = Path.Combine(sitePath, $"{DateTime.UtcNow.Ticks:x8}.log");
            var sb = new List<string>();
            Log(sb, e, 0);

            File.AppendAllLines(path, sb.ToArray());
            return Task.CompletedTask;
        }

        private static void Log(List<string> sb, Exception exception, int i)
        {
            if (exception == null)
            {
                return;
            }

            sb.Add("Exception Level: " + i);
            sb.Add(exception.GetType().FullName);
            sb.Add(exception.Message);
            sb.Add(exception.StackTrace);
            sb.Add("");
            sb.Add("");
            Log(sb, exception.InnerException, i + 1);
        }

        private static string SiteName(string siteName)
        {
            if (string.IsNullOrWhiteSpace(siteName))
            {
                siteName = "default";
            }

            return siteName.HashAsGuid().ToString("N");
        }
    }
}