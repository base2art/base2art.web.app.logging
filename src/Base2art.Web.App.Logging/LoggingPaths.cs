namespace Base2art.Web.App.Logging
{
    using System;
    using System.IO;
    using Web.App.Logging;

    public class LoggingPaths
    {
        public static string LogDir(string logType, string siteName, string baseFolder)
        {
            if (string.IsNullOrWhiteSpace(baseFolder))
            {
                baseFolder = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            }

            var sitePath = Path.Combine(baseFolder, "WebRequest", logType, SiteName(siteName));

            var dir = Directory.CreateDirectory(sitePath);

            return dir.FullName;
        }

        private static string SiteName(string siteName)
        {
            if (string.IsNullOrWhiteSpace(siteName))
            {
                siteName = "default";
            }

            return siteName.HashAsGuid().ToString("N");
        }
    }
}