namespace Base2art.Web.App.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using Aspects;
    using Aspects.Web;
    using Web.Http;

    public class RequestLogger : IHttpAspect
    {
        private static List<RequestLog> requestLines = new List<RequestLog>();
        private readonly string basePath;
        private readonly string siteName;

        public RequestLogger(string siteName, string basePath)
        {
            this.siteName = siteName;
            this.basePath = basePath;
        }

        public async Task OnInvoke(IMethodInterceptionArgs args, IHttpRequest request, IHttpResponse response, Func<Task> next)
        {
            var start = DateTime.UtcNow;
            await next();
            var end = DateTime.UtcNow;

            var requestLog = new RequestLog
                             {
                                 Started = start,
                                 Method = request.Method,
                                 Uri = request.Uri,
                                 UserAgent = this.Get(request.Headers, "User-Agent"),
                                 Referer = this.Get(request.Headers, "Referer"),
                                 User = this.FirstOf<IPrincipal>(args.Arguments),
                                 Status = response.StatusCode,
                                 TimeTaken = end - start
                             };

            requestLines.Add(requestLog);

            var oldLines = requestLines;
            requestLines = new List<RequestLog>();

            oldLines.ToArray().WriteDebug(this.Format, this.siteName, this.basePath);
        }

        private string Format(RequestLog requestLog) => string.Concat(requestLog?.Started,
                                                                      "\t",
                                                                      requestLog?.TimeTaken,
                                                                      "\t",
                                                                      requestLog?.Method ?? "GET",
                                                                      "\t",
                                                                      requestLog?.Uri,
                                                                      "\t",
                                                                      requestLog?.User?.Identity?.Name ?? "ANONYMOUS",
                                                                      "\t",
                                                                      requestLog?.Referer,
                                                                      "\t",
                                                                      requestLog?.UserAgent,
                                                                      "\t",
                                                                      requestLog?.Status).Replace("\r", "\n").Replace("\n", "\\n");

        private T FirstOf<T>(Arguments argsArguments) => argsArguments.OfType<T>().FirstOrDefault();

        private string Get(IReadOnlyDictionary<string, string> collection, string key)
        {
            if (collection.TryGetValue(key, out var value))
            {
                return value;
            }

            return null;
        }

        private class RequestLog
        {
            public DateTime Started { get; set; }
            public TimeSpan TimeTaken { get; set; }
            public IPrincipal User { get; set; }
            public Uri Uri { get; set; }
            public string Method { get; set; }
            public string UserAgent { get; set; }
            public string Referer { get; set; }
            public int Status { get; set; }
        }
    }
}