﻿namespace Base2art.Web.App.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Web.App.Configuration;

    public class Module : IModule
    {
        private readonly string siteName;

        public Module(string siteName) => this.siteName = siteName;

        public IReadOnlyList<IEndpointConfiguration> Endpoints => new IEndpointConfiguration[0];

        public IReadOnlyList<ITypeInstanceConfiguration> GlobalEndpointAspects
            => new ITypeInstanceConfiguration[]
               {
                   new CustomInjectionItemConfiguration
                   {
                       Type = typeof(RequestLogger),
                       Method = typeof(RequestLogger).GetMethod(nameof(RequestLogger.OnInvoke)),
                       Parameters = {{"siteName", this.siteName}}
                   }
               };

        public IReadOnlyList<ITaskConfiguration> Tasks => new ITaskConfiguration[0];

        public IReadOnlyList<ITypeInstanceConfiguration> GlobalTaskAspects => new ITypeInstanceConfiguration[0];

        public IHealthChecksConfiguration HealthChecks => null;

        public IApplicationConfiguration Application => new CustomApplicationConfiguration();

        public IReadOnlyList<IInjectionItemConfiguration> Injection
            => new IInjectionItemConfiguration[0];

        public IReadOnlyList<ICorsItemConfiguration> Cors => new ICorsItemConfiguration[0];

        private class CustomInjectionItemConfiguration : ICallableMethodConfiguration
        {
            public Dictionary<string, object> Parameters { get; } = new Dictionary<string, object>();
            public Dictionary<string, object> Properties { get; } = new Dictionary<string, object>();
            public Type Type { get; set; }
            public MethodInfo Method { get; set; }

            IReadOnlyDictionary<string, object> ITypeInstanceConfiguration.Parameters => this.Parameters;

            IReadOnlyDictionary<string, object> ITypeInstanceConfiguration.Properties => this.Properties;
        }

        private class CustomApplicationConfiguration : IApplicationConfiguration
        {
            public IReadOnlyList<ITypeInstanceConfiguration> Filters { get; } = new ITypeInstanceConfiguration[0];
            public IReadOnlyList<IModelBindingConfiguration> ModelBindings { get; } = new IModelBindingConfiguration[0];
            public IReadOnlyList<ITypeInstanceConfiguration> InputFormatters { get; } = new ITypeInstanceConfiguration[0];
            public IReadOnlyList<ITypeInstanceConfiguration> OutputFormatters { get; } = new ITypeInstanceConfiguration[0];
            public IReadOnlyDictionary<string, string> MediaTypes { get; } = new Dictionary<string, string>();
            public IExceptionConfiguration Exceptions { get; } = new CustomExceptionConfiguration();
        }

        private class CustomExceptionConfiguration : IExceptionConfiguration
        {
            public bool RegisterCommonHandlers { get; } = false;

            public bool AllowStackTraceInOutput { get; } = false;

//            public IReadOnlyList<ICallableMethodConfiguration> Handlers { get; }
            public IReadOnlyList<ITypeInstanceConfiguration> Loggers { get; }
        }
    }
}