namespace Base2art.Web.App.Logging
{
    using System;
    using System.Globalization;
    using System.Security.Cryptography;
    using System.Text;

    internal static class HashService
    {
        public static Guid HashAsGuid(this string value)
        {
            return Create(MD5.Create, value, x => new Md5HashResult(x)).AsGuid();
        }

        private static TResult Create<TResult>(Func<HashAlgorithm> algorithmProvider, string value, Func<byte[], TResult> creator)
            where TResult : HashResult
        {
            var bytes = Encoding.ASCII.GetBytes(value ?? string.Empty);
            using (var hashAlgorithm = algorithmProvider())
            {
                return creator(hashAlgorithm.ComputeHash(bytes));
            }
        }

        private class HashResult : IHashResult
        {
            public HashResult(byte[] hashedBytes) => this.HashedBytes = hashedBytes;

            protected byte[] HashedBytes { get; }

            public byte[] AsByteArray() => this.HashedBytes;

            public string AsString()
            {
                var hashedBytes = this.HashedBytes;
                var stringBuilder = new StringBuilder();
                for (var index = 0; index < hashedBytes.Length; ++index)
                {
                    stringBuilder.Append(hashedBytes[index].ToString("X2", CultureInfo.InvariantCulture));
                }

                return stringBuilder.ToString();
            }
        }

        private interface IHashResult
        {
            string AsString();

            byte[] AsByteArray();
        }

        private interface I16ByteHashResult : IHashResult
        {
            Guid AsGuid();
        }

        private class Md5HashResult : HashResult, I16ByteHashResult
        {
            public Md5HashResult(byte[] hashedBytes)
                : base(hashedBytes)
            {
            }

            public Guid AsGuid()
            {
                var hashedBytes = this.HashedBytes;
                return new Guid(new byte[16]
                                {
                                    hashedBytes[3],
                                    hashedBytes[2],
                                    hashedBytes[1],
                                    hashedBytes[0],
                                    hashedBytes[5],
                                    hashedBytes[4],
                                    hashedBytes[7],
                                    hashedBytes[6],
                                    hashedBytes[8],
                                    hashedBytes[9],
                                    hashedBytes[10],
                                    hashedBytes[11],
                                    hashedBytes[12],
                                    hashedBytes[13],
                                    hashedBytes[14],
                                    hashedBytes[15]
                                });
            }
        }
    }
}