namespace Base2art.Web.App.Logging
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;

    internal static class LogWriter
    {
        private static readonly ReaderWriterLock Locker = new ReaderWriterLock();

        public static bool ShouldLog(this Exception exception)
            => exception?.Data?.Contains("Base2art.Logged") != true;

        public static void WriteDebug<T>(this T[] objects, Func<T, string> formatter, string siteName, string baseFolder)
        {
            try
            {
                //You might wanna change timeout value 
                Locker.AcquireWriterLock((int) TimeSpan.FromMinutes(1).TotalMilliseconds);

                var sitePath = LoggingPaths.LogDir("Logs", siteName, baseFolder);
                var path = Path.Combine(sitePath, $"{DateTime.UtcNow:yyyyMMdd}.log");
                File.AppendAllLines(path, objects.Select(formatter));
            }
            catch
            {
            }
            finally
            {
                Locker.ReleaseWriterLock();
            }
        }
    }
}